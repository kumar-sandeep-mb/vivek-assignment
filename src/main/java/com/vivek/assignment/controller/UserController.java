package com.vivek.assignment.controller;

import com.vivek.assignment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/user")
    public ResponseEntity<?> getUsers(@RequestParam(required = false) Optional<Integer> age) {
        if(age.isPresent()) {
            if(age.get() < 1) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("age should be positive");
            } else {
                return ResponseEntity.ok(userService.getUsers(age.get()));
            }
        } else  {
            return ResponseEntity.ok(userService.getUsers());
        }
    }
}
