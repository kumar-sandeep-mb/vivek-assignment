package com.vivek.assignment.service;

import com.vivek.assignment.model.User;
import com.vivek.assignment.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserUtil userUtil;

    public List<User> getUsers(int age) {
        List<User> users = userUtil.getUsers();
        return users.stream().filter(user -> user.getAge() > age).collect(Collectors.toList());
    }

    public List<User> getUsers() {
        return userUtil.getUsers();
    }
}
