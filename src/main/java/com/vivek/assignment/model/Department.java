package com.vivek.assignment.model;

public enum Department {
    IT,
    HR,
    FINANCE
}
