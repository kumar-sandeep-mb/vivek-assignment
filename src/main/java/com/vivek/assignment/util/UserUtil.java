package com.vivek.assignment.util;

import com.vivek.assignment.model.Department;
import com.vivek.assignment.model.User;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserUtil {
    private List<User> users;

    public List<User> getUsers() {
        return new ArrayList<>(users);
    }

    @PostConstruct
    public void loadUsers() {
        User user1 = new User("Vivek", 25000d, 25, Department.IT);
        User user2 = new User("Shyam", 26000d, 35, Department.IT);
        User user3 = new User("Rahul", 30000d, 23, Department.HR);
        User user4 = new User("Harsh", 25000d, 31, Department.FINANCE);
        User user5 = new User("Sneha", 45000d, 27, Department.HR);
        User user6 = new User("Tanmay", 19000d, 25, Department.IT);
        User user7 = new User("Suresh", 27000d, 29, Department.IT);
        User user8 = new User("Vinay", 28000d, 28, Department.HR);
        User user9 = new User("Ishu", 20000d, 22, Department.FINANCE);
        User user10 = new User("Suraj", 23500d, 25, Department.IT);
        users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);
        users.add(user6);
        users.add(user7);
        users.add(user8);
        users.add(user9);
        users.add(user10);
    }
}
