package com.vivek.assignment;

import com.vivek.assignment.controller.UserController;
import com.vivek.assignment.service.UserService;
import com.vivek.assignment.util.UserUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class AssignmentApplicationTests {
    @Autowired
    private UserController userController;
    @Autowired
    private UserService userService;
    @Autowired
    private UserUtil userUtil;

    @Test
    void contextLoads() {
        assertThat(userController).isNotNull();
        assertThat(userService).isNotNull();
        assertThat(userUtil).isNotNull();
    }
}
